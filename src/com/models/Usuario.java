package com.models;

/**
 * Nomber de la clase: Usuario
 * Version 1.0
 * Fecha: 10/08/2018
 * Copyright ITCA-FEPADE
 * @author Juan Pablo Elias Hernández
 */
public class Usuario
{
    private String nombre;
    private String password;
    private int idTipo;

    public Usuario()
    {
        
    }

    public Usuario(String nombre, String password, int idTipo)
    {
        this.nombre = nombre;
        this.password = password;
        this.idTipo = idTipo;
    }
    

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public int getIdTipo()
    {
        return idTipo;
    }

    public void setIdTipo(int idTipo)
    {
        this.idTipo = idTipo;
    }
}
