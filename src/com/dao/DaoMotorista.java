package com.dao;

import com.connection.Conexion;
import com.models.Motorista;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.view.*;

/**
 * Nombre de la clase: DaoMotorista
 * Version: 2.0
 * Fecha: 10/08/2018
 * Copyright: ITCA-FEPADE
 * @author Kevin Lovos, Álvaro Pérez, Juan Pablo Elias
 */
public class DaoMotorista extends Conexion
{
    //<editor-fold defaultstate="collapsed" desc="Métodos de la clase">
    public List all()
    {
        List motoristas = new ArrayList();
        ResultSet res = null;
        try
        {
            this.conectar();
            String sql = "{CALL showMot()}";
            PreparedStatement pre = getConn().prepareCall(sql);
            res = pre.executeQuery();
            while (res.next())
            {
                Motorista mot = new Motorista();
                mot.setId(res.getInt(1));
                mot.setNombres(res.getString(2));
                mot.setApellidos(res.getString(3));
                mot.setEdad(res.getInt(4));
                mot.setGenero(res.getString(5));
                mot.setDui(res.getString(6));
                mot.setLicencia(res.getString(7));
                mot.setDireccion(res.getString(8));
                mot.setTelefono(res.getString(9));
                mot.setObsevaciones(res.getString(10));
                mot.setEstado(res.getInt(11));
                motoristas.add(mot);
            }
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al mostrar:\n" 
                    + e.toString(), "Error", 0);
        }
        finally
        {
            desconectar();
        }
        return motoristas;
    }
    
    public List find(String val)
    {
        List motoristas = new ArrayList();
        ResultSet res = null;
        try
        {
            this.conectar();
            String sql = "SELECT * FROM Motoristas WHERE estado != 0 " 
                    + "AND (dui LIKE '%" + val + "%' OR apellidos LIKE '%" + val + "%' "
                    + "OR nombres LIKE '%" + val + "%') ORDER BY id DESC;";
            PreparedStatement pre = getConn().prepareStatement(sql);
            res = pre.executeQuery();
            while (res.next())
            {
                Motorista mot = new Motorista();
                mot.setId(res.getInt(1));
                mot.setNombres(res.getString(2));
                mot.setApellidos(res.getString(3));
                mot.setEdad(res.getInt(4));
                mot.setGenero(res.getString(5));
                mot.setDui(res.getString(6));
                mot.setLicencia(res.getString(7));
                mot.setDireccion(res.getString(8));
                mot.setTelefono(res.getString(9));
                mot.setObsevaciones(res.getString(10));
                mot.setEstado(res.getInt(11));
                motoristas.add(mot);
            }
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al mostrar:\n" 
                    + e.toString(), "Error", 0);
        }
        finally
        {
            desconectar();
        }
        return motoristas;
    }
    
    public int add(Motorista mot)
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "{CALL addMot(?, ?, ?, ?, ?, ?, ?, ?, ?)}";
            PreparedStatement pre = getConn().prepareCall(sql);
            pre.setString(1, mot.getNombres());
            pre.setString(2, mot.getApellidos());
            pre.setInt(3, mot.getEdad());
            pre.setString(4, mot.getGenero());
            pre.setString(5, mot.getDui());
            pre.setString(6, mot.getLicencia());
            pre.setString(7, mot.getDireccion());
            pre.setString(8, mot.getTelefono());
            pre.setString(9, mot.getObsevaciones());
            if (pre.execute())
            {
                estado = 1;
            }
        } 
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al agregar el registro:\n" 
                    + e.toString(), "Error", 0);
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    
    public int edit(Motorista mot)
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "{CALL editMot(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
            PreparedStatement pre = getConn().prepareCall(sql);
            pre.setString(1, mot.getNombres());
            pre.setString(2, mot.getApellidos());
            pre.setInt(3, mot.getEdad());
            pre.setString(4, mot.getGenero());
            pre.setString(5, mot.getDui());
            pre.setString(6, mot.getLicencia());
            pre.setString(7, mot.getDireccion());
            pre.setString(8, mot.getTelefono());
            pre.setString(9, mot.getObsevaciones());
            pre.setInt(10, mot.getId());
            if (pre.execute())
            {
                estado = 1;
            }
        } 
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al editar el registro:\n" 
                    + e.toString(), "Error", 0);
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    
    public int delete(Motorista mot)
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "{CALL deleteMot(?)}";
            PreparedStatement pre = getConn().prepareStatement(sql);
            pre.setInt(1, mot.getId());
            if (pre.execute())
            {
                estado = 1;
            }
        } 
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al eliminar el registro:\n" 
                    + e.toString(), "Error", 0);
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    
    public void print()
    {
        JasperReport report;
        try
        {
            conectar();
            report = JasperCompileManager.compileReport("src/com/reports/RptMotoristas.jrxml");
            JasperPrint print = JasperFillManager.fillReport(report, null, getConn());
            JasperViewer.viewReport(print, false);
        }
        catch (JRException e)
        {
            JOptionPane.showMessageDialog(null, "Error al mostrar el reporte:\n"
                    + e.toString(), "Error", 0);
        }
    }
    
    public void printOne(Integer id)
    {
        JasperReport report;
        try
        {
            conectar();
            Map params = new HashMap();
            params.put("idM", id);
            report = JasperCompileManager.compileReport("src/com/reports/RptMotorista.jrxml");
            JasperPrint print = JasperFillManager.fillReport(report, params, getConn());
            JasperViewer.viewReport(print, false);
        }
        catch (JRException e)
        {
            JOptionPane.showMessageDialog(null, "Error al mostrar el reporte:\n"
                    + e.toString(), "Error", 0);
        }
    }
    //</editor-fold>
}
