package com.views;

import com.dao.DaoUsuario;
import com.models.Usuario;
import com.utils.Validations;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 * Nombre de la clase: FrmLogin
 * Version: 1.0
 * Fecha: 10/08/2018
 * Copyright ITCA-FEPADE
 * @author Juan Pablo Elias Hernández
 */
public class FrmLogin extends javax.swing.JFrame
{
    Usuario u = new Usuario();
    DaoUsuario daoU = new DaoUsuario();
    Validations val = new Validations();
    
    public FrmLogin()
    {
        initComponents();
        setLocationRelativeTo(this);
        setIconImage(new ImageIcon(getClass().getResource("/com/images/icon.png")).getImage());
    }
    
    // <editor-fold defaultstate="collapsed" desc="Métodos de la clase">
    private Boolean auth(String user)
    {
        return daoU.auth(user);
    }
    
    private void verify(String user, String pass)
    {
        u.setIdTipo(daoU.verify(user, pass));
    }
    
    private void login()
    {
        try
        {
            if (valid())
            {
                u.setNombre(jTxtUser.getText());
                if (auth(u.getNombre()))
                {
                    verify(jTxtUser.getText(), jTxtPass.getText());
                    if (u.getIdTipo() > 0)
                    {
                        FrmMenu menu = new FrmMenu(u.getIdTipo(), u.getNombre());
                        menu.setVisible(true);
                        this.dispose();
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "Contraseña incorrecta.\n"
                            + "Intentelo de nuevo.", "Mensaje", 
                            JOptionPane.INFORMATION_MESSAGE);
                    }
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "Usuario no encontrado.\n"
                            + "Intentelo de nuevo.", "Mensaje", 
                            JOptionPane.INFORMATION_MESSAGE);
                }
            }
            else
            {
                JOptionPane.showMessageDialog(null, "Ingrese su usuario y contraseña.", 
                        "Error", JOptionPane.WARNING_MESSAGE);
            }
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al ingresar al sistema.\n"
                        + e.toString(), "Error", 0);
        }
    }
    
    private Boolean valid()
    {
        if (val.IsNullOrEmpty(jTxtUser.getText()))
        {
            return false;
        }
        if (val.IsNullOrEmpty(jTxtPass.getText()))
        {
            return false;
        }
        return true;
    }
    // </editor-fold>
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPnlMain = new javax.swing.JPanel();
        jPnlLeft = new javax.swing.JPanel();
        jLblLoginIcon = new javax.swing.JLabel();
        jlblLeftTitle = new javax.swing.JLabel();
        jLblLeftSubtitle = new javax.swing.JLabel();
        jPnlRight = new javax.swing.JPanel();
        jLblUser = new javax.swing.JLabel();
        jTxtUser = new javax.swing.JTextField();
        jSptPass = new javax.swing.JSeparator();
        jLblPass = new javax.swing.JLabel();
        jTxtPass = new javax.swing.JPasswordField();
        jSptUser = new javax.swing.JSeparator();
        jLblRightTitle = new javax.swing.JLabel();
        jPnlEntrar = new javax.swing.JPanel();
        jLblEntrar = new javax.swing.JLabel();
        jLblSalir = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Iniciar Sesión");
        setName("FrmLogin"); // NOI18N
        setResizable(false);

        jPnlMain.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPnlLeft.setBackground(new java.awt.Color(52, 58, 64));
        jPnlLeft.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLblLoginIcon.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLblLoginIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/login_150px.png"))); // NOI18N
        jPnlLeft.add(jLblLoginIcon, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 90, 150, 170));

        jlblLeftTitle.setFont(new java.awt.Font("Raleway", 1, 30)); // NOI18N
        jlblLeftTitle.setForeground(new java.awt.Color(255, 255, 255));
        jlblLeftTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlblLeftTitle.setText("Bienvenido al Sistema");
        jPnlLeft.add(jlblLeftTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 280, 340, -1));

        jLblLeftSubtitle.setFont(new java.awt.Font("Quicksand", 0, 24)); // NOI18N
        jLblLeftSubtitle.setForeground(new java.awt.Color(255, 255, 255));
        jLblLeftSubtitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLblLeftSubtitle.setText("Control de Flota Vehicular");
        jPnlLeft.add(jLblLeftSubtitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 320, 320, 30));

        jPnlMain.add(jPnlLeft, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 380, 500));

        jPnlRight.setBackground(new java.awt.Color(254, 254, 254));
        jPnlRight.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLblUser.setFont(new java.awt.Font("Montserrat", 0, 14)); // NOI18N
        jLblUser.setForeground(new java.awt.Color(69, 77, 84));
        jLblUser.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLblUser.setText("Usuario:");
        jLblUser.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLblUser.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLblUser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLblUserMouseClicked(evt);
            }
        });
        jPnlRight.add(jLblUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 170, 80, -1));

        jTxtUser.setBackground(new java.awt.Color(254, 254, 254));
        jTxtUser.setFont(new java.awt.Font("Montserrat", 0, 16)); // NOI18N
        jTxtUser.setForeground(new java.awt.Color(69, 77, 84));
        jTxtUser.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtUser.setBorder(null);
        jTxtUser.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtUserKeyTyped(evt);
            }
        });
        jPnlRight.add(jTxtUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 190, 280, 30));

        jSptPass.setBackground(new java.awt.Color(254, 254, 254));
        jSptPass.setForeground(new java.awt.Color(69, 77, 84));
        jPnlRight.add(jSptPass, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 310, 280, 10));

        jLblPass.setFont(new java.awt.Font("Montserrat", 0, 14)); // NOI18N
        jLblPass.setForeground(new java.awt.Color(69, 77, 84));
        jLblPass.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLblPass.setText("Contraseña:");
        jLblPass.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLblPass.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLblPass.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLblPassMouseClicked(evt);
            }
        });
        jPnlRight.add(jLblPass, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 260, 110, -1));

        jTxtPass.setBackground(new java.awt.Color(254, 254, 254));
        jTxtPass.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        jTxtPass.setForeground(new java.awt.Color(69, 77, 84));
        jTxtPass.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtPass.setBorder(null);
        jTxtPass.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtPassKeyTyped(evt);
            }
        });
        jPnlRight.add(jTxtPass, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 280, 280, 30));

        jSptUser.setBackground(new java.awt.Color(254, 254, 254));
        jSptUser.setForeground(new java.awt.Color(69, 77, 84));
        jPnlRight.add(jSptUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 220, 280, 10));

        jLblRightTitle.setFont(new java.awt.Font("Raleway", 1, 38)); // NOI18N
        jLblRightTitle.setForeground(new java.awt.Color(69, 77, 84));
        jLblRightTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLblRightTitle.setText("INICIAR SESIÓN");
        jPnlRight.add(jLblRightTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 340, -1));

        jPnlEntrar.setBackground(new java.awt.Color(69, 77, 84));
        jPnlEntrar.setToolTipText("Entrar");
        jPnlEntrar.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLblEntrar.setFont(new java.awt.Font("Montserrat", 0, 24)); // NOI18N
        jLblEntrar.setForeground(new java.awt.Color(255, 255, 255));
        jLblEntrar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLblEntrar.setText("ENTRAR");
        jLblEntrar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLblEntrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLblEntrarMouseClicked(evt);
            }
        });
        jPnlEntrar.add(jLblEntrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 280, 50));

        jPnlRight.add(jPnlEntrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 360, 280, 50));

        jLblSalir.setFont(new java.awt.Font("Montserrat", 0, 14)); // NOI18N
        jLblSalir.setForeground(new java.awt.Color(69, 77, 84));
        jLblSalir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLblSalir.setText("Salir");
        jLblSalir.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLblSalir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLblSalir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLblSalirMouseClicked(evt);
            }
        });
        jPnlRight.add(jLblSalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 430, 60, -1));

        jPnlMain.add(jPnlRight, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 0, 380, 500));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPnlMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPnlMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLblUserMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLblUserMouseClicked
        
    }//GEN-LAST:event_jLblUserMouseClicked

    private void jLblPassMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLblPassMouseClicked
        
    }//GEN-LAST:event_jLblPassMouseClicked

    private void jLblSalirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLblSalirMouseClicked
        this.dispose();
    }//GEN-LAST:event_jLblSalirMouseClicked

    private void jLblEntrarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLblEntrarMouseClicked
        login();
    }//GEN-LAST:event_jLblEntrarMouseClicked

    private void jTxtUserKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtUserKeyTyped
        if (evt.getKeyChar() == KeyEvent.VK_ENTER)
        {
            login();
        }
    }//GEN-LAST:event_jTxtUserKeyTyped

    private void jTxtPassKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtPassKeyTyped
        if (evt.getKeyChar() == KeyEvent.VK_ENTER)
        {
            login();
        }
    }//GEN-LAST:event_jTxtPassKeyTyped

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmLogin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLblEntrar;
    private javax.swing.JLabel jLblLeftSubtitle;
    private javax.swing.JLabel jLblLoginIcon;
    private javax.swing.JLabel jLblPass;
    private javax.swing.JLabel jLblRightTitle;
    private javax.swing.JLabel jLblSalir;
    private javax.swing.JLabel jLblUser;
    private javax.swing.JPanel jPnlEntrar;
    private javax.swing.JPanel jPnlLeft;
    private javax.swing.JPanel jPnlMain;
    private javax.swing.JPanel jPnlRight;
    private javax.swing.JSeparator jSptPass;
    private javax.swing.JSeparator jSptUser;
    private javax.swing.JPasswordField jTxtPass;
    private javax.swing.JTextField jTxtUser;
    private javax.swing.JLabel jlblLeftTitle;
    // End of variables declaration//GEN-END:variables
}
