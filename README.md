# FLOTA VEHÍCULAR

### Proyecto de control de flota vehícular desarrollado en JAVA Swing y Mysql utilizando patron de diseño DAO

## Materia
Desarrollo de Aplicaciones Utilizando Tecnologías Emergentes.

## Integrantes

+ Juan Pablo Elias Hernández
+ Kevin Vladimir Lovos Romero
+ Álvaro Antonio Pérez Díaz
+ Verónica Maribel Martínez Jorge
+ Brayan Alexander Mejia Argueta

## Grupo
SIS-12 B
